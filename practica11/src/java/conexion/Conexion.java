/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package conexion;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import javax.swing.JOptionPane;

/**
 *
 * @author felipe
 */
public class Conexion {
    
    private final String URL = "jdbc:postgresql://localhost:5432/casino";
    private final String user = "postgres";
    private final String password = "Sonoco2016";
    private final String driver = "org.postgresql.Driver";
    protected Connection conn;
    private String query;
    private PreparedStatement qwe;
    private ResultSet resul;

 
    public Connection getConnection (){
        try {
            Class.forName(driver);                      
            conn = DriverManager.getConnection(URL,user,password);                          
        } catch(SQLException ex){ 
            JOptionPane.showMessageDialog(null, "Error "+ex);
            conn=null;
        } catch(ClassNotFoundException ex){
            JOptionPane.showMessageDialog(null, "Error "+ex);
            conn=null;
            
        }
       // System.out.println("conexion exitosa");
        return conn;
        
    }
    public void desconectar (){
        try {
            if (conn != null) {
                conn.close();
            }

        } catch (SQLException ex) {

        }
        
    }
    
    public void Errorsql(SQLException ex) {
        System.out.println("sql Mensje " + ex.getMessage());
        System.out.println("sql Estado " + ex.getSQLState());
        System.out.println("sql errorCodigo " + ex.getErrorCode());
        //error de sql 

    }
}
